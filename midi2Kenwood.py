#coding:utf-8
'''

v1.0 P. Nouchi - F6IFY le 11 Avril 2021
    - Background is grey when RIT or VFO is inactiv
    - Add RIT in the display window
    - this version (midi2kenwood.py) is only for the kenwood TRXs
v0.9 P. Nouchi - F6IFY le 7 Avril 2021
    - Add VFO frequency display window
v0.8 P. Nouchi - F6IFY le 18 Mars 2021
    - try multithreading
v0.7 P. Nouchi - F6IFY le 17 Mars 2021
    - Works for the TS590 @ F6BEE
v0.6 P. Nouchi - F6IFY le 13 Mars 2021
    - VFOs and RIT ok for the SunSDR
v0.5 P. Nouchi - F6IFY le 13 Mars 2021
    - correct bug to read the mode
v0.4 P. Nouchi - F6IFY le 12 Mars 2021
    - Bug correction for Icom TRX
v0.3 P. Nouchi - F6IFY le 11 Mars 2021
    - Rename files and add mode of the kenwood radio
v0.2 P. Nouchi - F6IFY le 10 Mars 2021
    - Add code for kenwood and Icom transceivers
v0.1 P. Nouchi - F6IFY le 9 Mars 2021
    - Git on Bitbucket.org
    - start from Patrick EGLOFF - TK5EP sources

 reads MIDI commands from a DJ console and sends commands to a Icom IC7610 via a COM port
 command includes the device number (0 to 4, depending on your setup),
 the MIDI status (depending on the kind of control),
 the control ID (depending on the device) and finally the control value
'''
# import libraries
import configparser  # https://docs.python.org/3/library/configparser.html
import os
import struct
import sys
import math
import time
# import pygame              #http://www.pygame.org/docs/ref/midi.html
import pygame.midi
import serial  # https://pythonhosted.org/pyserial/
from serial import SerialException
import tkinter
import threading
import keyboard


# Config = configparser.ConfigParser()
Config = configparser.ConfigParser(
    allow_no_value=True)  # allow_no_value=True -> to allow adding comments without value, so no trailing =


########################################
# Read the ini file and get settings
# datas are strings so need to be converted !
########################################
def ReadIniFile():
    try:
        # declare global variables
        global radioModel
        global trxAddress
        global radioSpeed
        global radioPort
        global radioBits
        global radioStop
        global radioParity
        global radioXonXoff
        global radioRtsCts
        global radioDsrDtr
        global midiDeviceIn
        global midiDeviceOut
        global radioDefaultMode
        global radioDefaultVFO
        # global RadioFiltre
        global radioMode
        global debug
        global kenwood
        global vfoStep
        global noAction
        global preamble
        global postamble
        global trxAddress
        global icomRITValue
        global RITStep
        global noAction
        global vfo
        global ritFreq

        # read ini file
        Config.read('midi2Cat.ini')

        # check if Midi section exists
        if Config.has_section('Midi'):
            midiDeviceIn = Config.getint('Midi', 'deviceIN')  # get the device Midi IN, reads and converts in INT
            midiDeviceOut = Config.getint('Midi', 'deviceOUT')  # get the device Midi OUT
        else:
            print("Midi section missing in config file. Please correct this !")
            sys.exit(1)

        # check if default section exists
        if Config.has_section('Default'):
            radioDefaultMode = Config.get('Default', 'mode')
            radioDefaultVFO = Config.get('Default', 'VFO')
            vfoStep = int(Config.get('Default', 'vfoStep'))
            vfo = radioDefaultVFO
        else:
            print("Default section missing in config file. Please correct this !")
            sys.exit(1)

        # check if Radio section exists
        if Config.has_section('Radio'):
            radioModel = Config.get('Radio', 'model')
            if radioModel == 'ic756pro3':
                preamble = b'\xfe\xfe\x6e\xe0'
                kenwood = False
                trxAddress = b"\x6e"
            elif radioModel == 'ic7610':
                preamble = b"\xFE\xFE\x98\xE0"  # ic7610
                trxAddress = b"\x98"
                kenwood = False
            elif radioModel == 'ts590':
                kenwood = True
            elif radioModel == "kenwood":
                kenwood = True
            elif radioModel == "SDRConsole":
                kenwood = True

            radioPort = Config.get('Radio', 'comport')
            radioSpeed = Config.getint('Radio', 'speed')  # reads and converts in INT
            radioBits = Config.getint('Radio', 'bits')
            radioStop = Config.getint('Radio', 'stop')
            radioParity = Config.get('Radio', 'parity')
            radioXonXoff = Config.getint('Radio', 'xonxoff')
            radioRtsCts = Config.getint('Radio', 'rtscts')
            radioDsrDtr = Config.getint('Radio', 'dsrdtr')
            print("Radio model", radioModel, "Comport=", radioPort, "Speed=", radioSpeed, "Bits=", radioBits, "Stop=",
                  radioStop, "Parity=", radioParity)
        else:
            print("Radio section missing in config file. Please correct this !")
            sys.exit(1)

        # check if Commands section exists
        if Config.has_section('Commands'):
            # print ("Commands section OK")
            # extraction des commandes avec la commande .partition('.')
            debug = Config.getint('Commands', 'debug')
            print('debug Value is %d' % debug)
        else:
            print("Commands section missing in config file. Please correct this !")
            sys.exit(1)

    # if an option is missing, raise an error
    except configparser.NoOptionError:
        print(
            "Missing option(s) in config file !\nPlease correct this or remove file to allow creating a default one !")
        sys.exit(1)


    except configparser.NoSectionError:
        print ("Missing section(s) in config file !\nPlease correct this or remove file to allow creating a default one !")
        sys.exit(1)


########################################
# create_midi2Cat_inifile
# in case the ini file does not exist, create one with default values
########################################
def CreateIniFile():
    inifile = open("midi2Cat.ini", 'w')

    # add default section
    Config.add_section('Default')
    Config.set('Default', '# midi2Cat config file')
    Config.set('Default', '# defaults values created by program')
    Config.set('Default', 'mode', 'USB')
    Config.set('Default', 'VFO', 'A')
    # add section Midi
    Config.add_section('Midi')
    # add settings
    Config.set('Midi', 'deviceIN', '1')
    Config.set('Midi', 'deviceOUT', '3')

    # add section Radio
    Config.add_section('Radio')
    # add settings
    Config.set('Radio', 'model', 'ts590')
    Config.set('Radio', 'comport', 'COM2')
    Config.set('Radio', 'speed', '115200')
    Config.set('Radio', 'bits', '8')
    Config.set('Radio', 'stop', '1')
    Config.set('Radio', 'parity', 'N')
    Config.set('Radio', 'xonxoff', '0')
    Config.set('Radio', 'rtscts', '0')
    Config.set('Radio', 'dsrdtr', '0')

    # add section Commands
    Config.add_section('Commands')
    # add settings
    Config.set('Commands', 'debug', 0)
    Config.set('Commands', '14412', 'TS0;')
    Config.set('Commands', '144.3', 'FR0;FT1;')

    # and lets write it out...
    Config.write(inifile)
    inifile.close()

    # now read the config file
    ReadIniFile()


########################################
# send data to radio
########################################
def SendToRadio(strCat):
    radioSer.write(strCat.encode())


def SendString(string, length):  # Write a string list
    count = 0
    reply = ""
    while (count < length):
        senddata = int(bytes(string[count], 'UTF-8'), 16)
        reply = radioSer.write(struct.pack('>B', senddata))
        count += 1


########################################
# get mode from radio
########################################
def GetModeFromRadiokenwood():
    mode = 'unknown'
    strCat = 'MD;'
    SendToRadio(strCat)

    while mode == 'unknown':
        codeReceived = radioSer.read(3)
        if codeReceived == b'':
            SendToRadio(strCat)
        elif codeReceived == b'ID;':
            SendToRadio('ID020;')
        elif codeReceived == b'MD1':
            mode = 'LSB'
        elif codeReceived == b'MD2':
            mode = 'USB'
        elif codeReceived == b'MD3':
            mode = 'CW'
        elif codeReceived == b'MD4':
            mode = 'FM'
        elif codeReceived == b'MD5':
            mode = 'AM'
        elif codeReceived == b'MD6':
            mode = 'FSK'
        elif codeReceived == b'MD7':
            mode = 'CWR'
        else:
            mode = 'unknown'
    print('Mode is ' + mode)
    return mode




########################################
# Set frequency
########################################
def SetFrequencykenwood(dwFrequency, vfo):
    f = str("%011d" % dwFrequency)
    strCat = 'F' + vfo + f + ';'
    if debug >= 1:
        print('Command Sent is ' + strCat)
    SendToRadio(strCat)


########################################
# change radio mode and filtre number
########################################
def ChangeModekenwood(mode, midi_out):
    global radioMode
    if mode == 'CW':
        strCat = 'MD3;MD;'  # MODE = CW
        radioMode = 'CW'
        # switch on backlight
        midi_out.write([[[144, 49, 127], 0], [[144, 50, 0], 0], [[144, 51, 0], 0], [[144, 52, 0], 0]])
    elif mode == 'LSB':
        strCat = 'MD1;MD;'
        radioMode = 'SSB'
        # switch on backlight
        midi_out.write([[[144, 51, 127], 0], [[144, 49, 0], 0], [[144, 50, 0], 0], [[144, 52, 0], 0]])
    elif mode == 'USB':
        strCat = 'MD2;MD;'
        radioMode = 'SSB'
        # switch on backlight
        midi_out.write([[[144, 51, 0], 0], [[144, 49, 0], 0], [[144, 50, 127], 0], [[144, 52, 0], 0]])
    elif mode == 'FSK':
        strCat = 'MD6;MD;'
        radioMode = 'FSK'
        # switch on backlight
        midi_out.write([[[144, 52, 127], 0], [[144, 49, 0], 0], [[144, 50, 0], 0], [[144, 51, 0], 0]])

    SendToRadio(strCat)


########################################
# put radio on VFO A or B
########################################
def ChangeVFO(vfo, midi_out):
    if vfo == 'A':
        strCat = 'FR0;'
        label_vfoA.config(bg='#4065A4', fg='yellow')
        label_vfoB.config(bg="grey", fg="white")
        # switch on backlight
        midi_out.write([[[144, 35, 127], 0], [[144, 33, 0], 0], [[144, 3, 0], 0], [[144, 4, 0], 0]])
    elif vfo == 'B':
        strCat = 'FR1;'
        label_vfoB.config(bg='#4065A4', fg='yellow')
        label_vfoA.config(bg="grey", fg="white")
        # switch on backlight
        midi_out.write([[[144, 33, 127], 0], [[144, 35, 0], 0], [[144, 3, 0], 0], [[144, 4, 0], 0]])
    SendToRadio(strCat)
    label_vfoB.update()
    label_vfoA.update()


########################################
# put RIT OFF
########################################
def PutRITOFF(midi_out):
    radioSer.write(b'RT0;')
    midi_out.write([[[144, 83, 127], 0], [[144, 82, 0], 0], [[144, 81, 0], 0]])

    label_rit.config(bg="grey", fg="white")
    label_rit.update()


########################################
# put RIT ON
########################################
def PutRITON(midi_out):
    radioSer.write(b'RT1;')
    midi_out.write([[[144, 81, 127], 0], [[144, 82, 0], 0], [[144, 83, 0], 0]])
    label_rit.config(bg='#4065A4', fg='yellow')
    label_rit.update()

########################################
# Clear RIT
# ########################################
def ClearRIT(midi_out):
    radioSer.write(b'RC;')
    midi_out.write([[[144, 82, 127], 0]])  # CUE-B backlight ON


def setRIT(ritFreq, Up):
    f = str("%05d" % abs(ritFreq))
    if Up:
        strCat = 'RU' + f + ';'  # RIT offset > 0
    else:
        strCat = 'RD' + f + ';'  # RIT offset < 0
    if debug >= 1:
        print('Command Sent is ' + strCat)
    SendToRadio(strCat)


########################################
# start of program
########################################
# check if ini file exists and read the settings
inifile = 'midi2Cat.ini'
if os.path.isfile(inifile):
    ReadIniFile()
else:
    print("Configuration file does not exist, creating it")
    CreateIniFile()

#######################################
# define the serial port for radio
#######################################
try:
    radioSer = serial.Serial(
        # set the port parameters
        port=radioPort,
        baudrate=radioSpeed,
        bytesize=radioBits,
        stopbits=radioStop,
        parity=radioParity,
        xonxoff=radioXonXoff,  # software flow control
        rtscts=radioRtsCts,  # hardware (RTS/CTS) flow control
        dsrdtr=radioDsrDtr,  # hardware (DSR/DTR) flow control
        timeout=0.1)

    # open radio port
    # test if it is open
    ok = radioSer.is_open
    print('Is the Com open : %s ' % ok)
# radioSer.close()

# if COM port communication problem
except SerialException:
    print("No communication to", radioPort, ", check configuration file and available ports on computer !")
    sys.exit(1)


#####################################
# init the Midi device
#####################################
def InitMidiDevice(midiDeviceIn):
    pygame.midi.init()

    # testing if device is already opened
    if pygame.midi.get_device_info(midiDeviceIn)[4] == 1:
        print('Error: Can''t open the MIDI device - It''s already opened!')
        sys.exit(1)

    # testing if input device is an input
    if pygame.midi.get_device_info(midiDeviceIn)[3] == 1:
        print('Error: Midi input device selected in not an input ! Please correct this.')
        sys.exit(1)

    # testing if input device is an input
    if pygame.midi.get_device_info(midiDeviceOut)[3] == 0:
        print('Error: Midi output device selected in not an output ! Please correct this')
        sys.exit(1)

        # count the number of Midi devices connected
    MidiDeviceCount = pygame.midi.get_count()
    print("Number of Midi devices found =", MidiDeviceCount)

    # display the default Midi device
    print("\nWindows default Midi input device =", pygame.midi.get_default_input_id())
    print("Windows default Midi output device =", pygame.midi.get_default_output_id())

    # display Midi device selected by config file
    print("\nConfig file selected Midi INPUT device =", midiDeviceIn)
    print("Config file selected Midi OUTPUT device =", midiDeviceOut)

    # display infos about the selected Midi device
    print("\nInfos about Midi devices :")
    for n in range(pygame.midi.get_count()):
        print(n, pygame.midi.get_device_info(n))

    # open Midi device
    # my_input = pygame.midi.Input(midiDeviceIn)


###########################################
# all backlights on Midi console
###########################################
def ledsON(midi_out):
    midi_out.write([[[144, 1, 127], 0], [[144, 2, 127], 0], [[144, 3, 127], 0], [[144, 4, 127], 0], [[144, 33, 127], 0],
                    [[144, 34, 127], 0], [[144, 35, 127], 0], [[144, 49, 127], 0], [[144, 50, 127], 0],
                    [[144, 51, 127], 0], [[144, 52, 127], 0], [[144, 81, 127], 0], [[144, 82, 127], 0],
                    [[144, 83, 127], 0], [[144, 43, 127], 0], [[144, 45, 127], 0], [[144, 48, 127], 0]])


###########################################
# all backlights on Midi console
###########################################
def ledsOFF(midi_out):
    midi_out.write(
        [[[144, 1, 0], 0], [[144, 2, 0], 0], [[144, 3, 0], 0], [[144, 4, 0], 0], [[144, 33, 0], 0], [[144, 34, 0], 0],
         [[144, 35, 0], 0], [[144, 49, 0], 0], [[144, 50, 0], 0], [[144, 51, 0], 0], [[144, 52, 0], 0],
         [[144, 81, 0], 0], [[144, 82, 0], 0], [[144, 83, 0], 0], [[144, 43, 0], 0], [[144, 45, 0], 0],
         [[144, 48, 0], 0]])


###########################################
# all backlights on Midi console
###########################################
def blinkLED(numTimes, speed, midi_out):
    for i in range(0, numTimes):  ## Run loop numTimes
        ledsON(midi_out)
        time.sleep(speed)
        ledsOFF(midi_out)
        time.sleep(speed)


#################################
# Increment or Decrement the VFO of kenwood TRX
#################################
def UpKenwood(vfoStep, vfo):
    if vfo == 'A':
        f = update_freq()
    else:
        f = update_freqB()
    f += vfoStep
    SetFrequencykenwood(f, vfo)


def DownKenwood(vfoStep, vfo):
    if vfo == 'A':
        f = update_freq()
    else:
        f = update_freqB()

    f -= vfoStep
    SetFrequencykenwood(f, vfo)


def GetfreqKenwood(vfo):
    if vfo == 'A':
        freq = float(var_freqA.get())
    else:
        freq = float(var_freqB.get())
        return freq


########################################
# main loop read Midi, convert and send to radio
########################################
# http://stackoverflow.com/questions/15768066/reading-piano-notes-on-python/24821493#24821493
def readInput(input_device, midi_out, vfoStep):
    noAction = 0
    ACTION = 10
    RITStep = 5
    global vfo
    loop = 0
    while loop < ACTION:
        loop += 1
        if input_device.poll():
            event = input_device.read(1)[0]
            if debug >= 2: print(event)
            data = event[0]
            event[0] = []
            # print (data)
            # timestamp = event[1]
            device = data[0]
            status = data[1]
            control = data[2]
            value = data[3]
            # for debugging
            # print (device)
            # print(status)
            # print (control)
            # print (value)
            modeReceived = ""
            # detect rotation of jogs and pots
            if device == 176:
                if radioModel == 'ts590':
                    if status == 48:
                        if noAction < ACTION:  # 1 Action on 20 increment of the Jog
                            noAction += 1
                        if noAction >= ACTION:
                            noAction = 0
                            if control < 64:  # VFO UP
                                UpKenwood(vfoStep, vfo)
                            else:  # VFO Down
                                DownKenwood(vfoStep, vfo)
                    elif status == 49:
                        if noAction < ACTION:  # 1 Action on xx increment of the Jog
                            noAction += 1
                        if noAction >= ACTION:
                            noAction = 0
                            rx_rit = var_rit.get()
                            ritFreq = int(rx_rit[5:9])
                            if control > 64:
                                ritFreq -= RITStep
                                radioSer.write(b'RD00005;')  # RIT DOWN
                            else:
                                ritFreq += RITStep
                                radioSer.write(b'RU00005;')  # RIT UP
                            if debug == 1: print('RIT is %5d' % ritFreq)
                    elif status == 54:  # SLIDER
                        strCat = format("AG%04d;" % (control))  # RX VOLUME
                        SendToRadio(strCat)
                    elif status == 60 and radioMode == 'CW':  # CW bandwidth
                        print("%04d" % (math.floor(100 + (1000 - 100) * control / 127)))
                        strCat = format("FW%04d;" % (math.floor(100 + (1000 - 100) * control / 127)))
                        SendToRadio(strCat)
                    elif status == 60 and radioMode == 'FSK':
                        print("%04d" % (math.floor(250 + (1500 - 250) * control / 127)))
                        strCat = format("FW%04d;" % (math.floor(250 + (1500 - 250) * control / 127)))
                        SendToRadio(strCat)
                elif kenwood:
                    if status == 49:
                        if noAction < ACTION:  # 1 Action on xx increment of the Jog
                            noAction += 1
                        if noAction >= ACTION:
                            noAction = 0
                            if control > 64:
                                ritFreq -= RITStep
                                if ritFreq >= 0:
                                    ritDirection = True
                                else:
                                    ritDirection = False

                                # radioSer.write(b'RD00010;')  # RIT DOWN
                            else:
                                ritFreq += RITStep
                                if ritFreq >= 0:
                                    ritDirection = True
                                else:
                                    ritDirection = False
                            setRIT(ritFreq, ritDirection)
                                # radioSer.write(b'RU00010;')  # RIT UP
                            if debug == 2: print('RIT is %5d' % ritFreq)
                    elif status == 48:
                        if noAction < ACTION:  # 1 Action on 20 increment of the Jog
                            noAction += 1
                        if noAction >= ACTION:
                            noAction = 0
                            if control < 64:  # VFO UP
                                UpKenwood(vfoStep, vfo)
                            else:  # VFO Down
                                DownKenwood(vfoStep, vfo)

            # detects buttons
            if device == 144:
                if status == 1 and control == 127:
                    pass
                elif status == 2 and control == 127:
                    pass
                elif status == 3:
                    if radioModel == 'ts590':
                        return
                    elif kenwood:
                        if control > 64:  # PTT On
                            strCat = 'TX0;TX0;'
                        else:
                            strCat = 'RX;'  # PTT Off

                    SendToRadio(strCat)
                    # midi_out.write([[[144, 3, 127], 0], [[144, 4, 0], 0], [[144, 34, 127], 0], [[144, 35, 127], 0]])
                elif status == 4 and control == 127:  # Read Frequency
                    pass
                elif status == 48 and control == 127:  # Ask mode
                    modeReceived = GetModeFromRadiokenwood()
                    print(modeReceived)
                elif status == 35 and control == 127:  # VFO A
                    vfo = 'A'
                    ChangeVFO(vfo, midi_out)
                elif status == 33 and control == 127:  # VFO B
                    vfo = 'B'
                    ChangeVFO(vfo, midi_out)
                elif status == 34:  # CUE key pressed
                    if control == 127:  # A = B
                        # radioSer.write(b'AB;') ne fonctionne pas sur SunSDR
                        freq = update_freq()
                        SetFrequencykenwood(freq, 'B')
                        update_freqB()

                elif status == 43 and control == 127:  # if REC key pressed
                    pass
                elif status == 45 and control > 64:  # Automix key pressed
                    if vfoStep == 10:  # Change Step in Jog A (VFO)
                        vfoStep = 100
                    else:
                        vfoStep = 10

                elif status == 49 and control == 127:
                    mode = 'CW'
                    ChangeModekenwood(mode, midi_out)
                    # switch on backlight
                    midi_out.write([[[144, 49, 127], 0], [[144, 50, 0], 0], [[144, 51, 0], 0], [[144, 52, 0], 0]])
                elif status == 50 and control == 127:
                    mode = 'USB'
                    ChangeModekenwood(mode, midi_out)
                    midi_out.write([[[144, 50, 127], 0], [[144, 49, 0], 0], [[144, 51, 0], 0], [[144, 52, 0], 0]])
                elif status == 51 and control == 127:  # MODE = USB
                    mode = 'LSB'
                    ChangeModekenwood(mode, midi_out)
                    midi_out.write([[[144, 51, 127], 0], [[144, 50, 0], 0], [[144, 49, 0], 0], [[144, 52, 0], 0]])
                elif status == 52 and control == 127:
                    mode = 'FSK'
                    ChangeModekenwood(mode, midi_out)
                    # switch on backlight
                    midi_out.write([[[144, 52, 127], 0], [[144, 49, 0], 0], [[144, 50, 0], 0], [[144, 51, 0], 0]])

                elif status == 81 and control == 127:
                    PutRITON(midi_out)
                elif status == 83 and control == 127:
                    PutRITOFF(midi_out)
                elif status == 82 and control == 127:
                    ClearRIT(midi_out)
                    ritFreq = 0


def getVFOActif():
    strCat = "FR;"
    radioSer.write(strCat.encode())
    lineSDR = ""
    s = ""
    while s != ";":
        s = radioSer.read().decode("utf-8")
        lineSDR += s
    if lineSDR[2] == "0":   # vfo A is activ
        vfo = "A"
        label_vfoA.config(bg='#4065A4', fg='yellow')
        label_vfoB.config(bg="grey", fg="white")
    else:
        vfo = "B"
        label_vfoB.config(bg='#4065A4', fg='yellow')
        label_vfoA.config(bg="grey", fg="white")
    label_vfoA.update()
    label_vfoB.update()
    return vfo

def update_freq(*args):
    strCat = 'FA;'      # ask the frequency
    header = "FA"
    radioSer.write(strCat.encode())
    lineSDR = ""
    s = ""
    while s != ";":
        s = radioSer.read().decode("utf-8")
        lineSDR += s

    if lineSDR[0:2] == header:
        # print(lineSDR)
        rx_freq = lineSDR[3:13]
        f = int(rx_freq)
        fa = f / 1000
        strFreq = str("A: %6.3f" % fa)
        if debug == 2: print(strFreq)
        var_freqA.set(strFreq)
        label_vfoA.configure(text=strFreq)
        label_vfoA.update()
    else:
        f = 0
    return f


def update_freqB(*args):
    strCat = 'FB;'      # ask the frequency
    radioSer.write(strCat.encode())
    lineSDR = ""
    s = ""
    while s != ";":
        s = radioSer.read().decode("utf-8")
        lineSDR += s

    if lineSDR[0:2] == 'FB':
        # print(lineSDR)
        rx_freqB = lineSDR[3:13]
        f = int(rx_freqB)
        fb = f / 1000
        strFreqB = str("B: %6.3f" % fb)
        if debug == 2: print(strFreqB)
        var_freqB.set(strFreqB)
        label_vfoB.configure(text=strFreqB)
        label_vfoB.update()
    else:
        f = 0
    return f


def polling(*args):
    InitMidiDevice(midiDeviceIn)
    my_input = pygame.midi.Input(midiDeviceIn)  # open Midi device
    midi_out = pygame.midi.Output(midiDeviceOut)
    blinkLED(2, 0.3, midi_out)
    update_freq()
    update_freqB()
    getRITFreq()
    getVFOActif()
    while not keyboard.is_pressed("e"):
        #update_freq()
        #update_freqB()
        getRITFreq()

        readInput(my_input, midi_out, vfoStep)
        #time.sleep(0.1)


def getRITFreq(*args):
    strCat = 'IF;'  # ask the RIT frequency
    radioSer.write(strCat.encode())
    lineSDR = ""
    s = ""
    while s != ";":
        s = radioSer.read().decode("utf-8")
        lineSDR += s

    if lineSDR[0:2] == 'IF':
        # print(lineSDR)
        rx_rit = str(lineSDR[18:23])
        if debug == 1: print(rx_rit)
        if int(rx_rit) >= 0:
            var_rit.set("RIT:+" + rx_rit + " Hz")
        else:
            var_rit.set("RIT: " + rx_rit + " Hz")
        #label_rit.configure(text="RIT: " + rx_rit)
        label_rit.update()
        if debug == 2: print("RIT: " + rx_rit)
        # Now check if RIT status
        if lineSDR[23] == "0":  # RIT is Off
            label_rit.config(bg="grey", fg="white")
        else:       # RIT is On
            label_rit.config(bg='#4065A4', fg='yellow')
    else:
        rx_rit = "xxxx"
    return rx_rit


def djcontrolInit():
    if radioModel != "SDRConsole":  # because SDRConsole manage the midi console
        InitMidiDevice(midiDeviceIn)
        my_input = pygame.midi.Input(midiDeviceIn)  # open Midi device
        # midi_out = pygame.midi.Output(midiDeviceOut)
        # blink all backlights as a test
        blinkLED(2, 0.3)
        return my_input

######################
# Let's start !
######################
if __name__ == '__main__':
    screen_vfo = tkinter.Tk()
    screen_vfo.title("VFO A & B")
    screen_vfo.geometry("550x300")
    screen_vfo.minsize(500, 250)
    screen_vfo.config(background='#4065A4')

    frameA = tkinter.Frame(screen_vfo, bg='#4065A4', bd=1, relief=tkinter.SUNKEN)
    frameB = tkinter.Frame(screen_vfo, bg='#4065A4', bd=1, relief=tkinter.SUNKEN)
    frameC = tkinter.Frame(screen_vfo, bg='#4065A4', bd=1, relief=tkinter.SUNKEN)

    var_freqA = tkinter.StringVar()
    var_freqA.trace_add("write", update_freq)

    var_freqB = tkinter.StringVar()
    var_freqB.trace_add("write", update_freqB)

    var_rit = tkinter.StringVar()
    var_rit.trace_add("write", getRITFreq)
    """
    isPolling = BooleanVar()
    isPolling.set(True)
    checkA = Checkbutton(screen_vfo, text="vfo-A", offvalue=False, onvalue=True, variable=isPolling, bg='#4065A4', fg='yellow', font=("courrier", 14))
    checkA.pack()
    isPollingB = BooleanVar()
    isPollingB.set(True)
    checkB = Checkbutton(screen_vfo, text="vfo-B", offvalue=False, onvalue=True, variable=isPollingB, bg='#4065A4', fg='yellow', font=("courrier", 14))
    checkB.pack()
    """
    label_vfoA = tkinter.Label(frameA, textvariable=var_freqA, font=("courrier", 50), bg='#4065A4', fg='yellow')
    label_vfoA.pack(expand=1)

    label_vfoB = tkinter.Label(frameB, textvariable=var_freqB, font=("courrier", 50), bg='#4065A4', fg='yellow')
    label_vfoB.pack(expand=1)

    label_rit = tkinter.Label(frameC, textvariable=var_rit, font=("arial", 24), bg='#4065A4', fg='yellow')
    label_rit.pack(expand=1)

    frameA.pack(expand=1)
    frameB.pack(expand=1)
    frameC.pack(expand=1)
    """
    # create menu
    menu_bar = Menu(screen_vfo)
    file_menu = Menu(menu_bar, tearoff=0)
    file_menu.add_command(label="Poll frequency", command=update_freq)

    # file_menu.add_checkbutton(label="polling ?", offvalue=False, onvalue=True, variable=isPolling)
    file_menu.add_separator()
    # file_menu.add_command(label="Mode", command=GetModeFromRadiokenwood)
    file_menu.add_command(label="Quit", command=screen_vfo.quit)
    menu_bar.add_cascade(label="Function", menu=file_menu)
    screen_vfo.config(menu=menu_bar)
    
    vfoA = True
    update_freq(vfoA)
    update_freqB()
    """
    screen_vfo.after(1, polling())
    screen_vfo.mainloop()

print('Fin du Programme')



